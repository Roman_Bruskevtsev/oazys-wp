<?php
/**
 *
 * @package WordPress
 * @subpackage OazysDah
 * @since 1.0
 * @version 1.0
 */
get_header(); 

if ( post_password_required() ) :
    get_template_part( 'template-parts/password-content' );
else :
    if( have_rows('content') ):
        while ( have_rows('content') ) : the_row();
            if( get_row_layout() == 'fullsreen_banner' ): 
                get_template_part( 'inc/acf-content/fullsreen_banner' );
            elseif( get_row_layout() == 'fullscreen_slider' ): 
                get_template_part( 'inc/acf-content/fullscreen_slider' );
            elseif( get_row_layout() == 'products_categories_section' ): 
                get_template_part( 'inc/acf-content/products_categories_section' );
            elseif( get_row_layout() == 'fullscreen_image' ): 
                get_template_part( 'inc/acf-content/fullscreen_image' );
            elseif( get_row_layout() == 'image_content_section' ): 
                get_template_part( 'inc/acf-content/image_content_section' );
            elseif( get_row_layout() == 'content_image_section' ): 
                get_template_part( 'inc/acf-content/content_image_section' );
            elseif( get_row_layout() == 'testimonials_section' ): 
                get_template_part( 'inc/acf-content/testimonials_section' );
            elseif( get_row_layout() == 'contact_section' ): 
                get_template_part( 'inc/acf-content/contact_section' );
            elseif( get_row_layout() == 'partners_section' ): 
                get_template_part( 'inc/acf-content/partners_section' );
            elseif( get_row_layout() == 'services_section' ): 
                get_template_part( 'inc/acf-content/services_section' );
            elseif( get_row_layout() == 'content_section' ): 
                get_template_part( 'inc/acf-content/content_section' );
            elseif( get_row_layout() == 'content_section_no_background' ): 
                get_template_part( 'inc/acf-content/content_section_no_background' );
            elseif( get_row_layout() == 'contacts_section' ): 
                get_template_part( 'inc/acf-content/contacts_section' );
            elseif( get_row_layout() == 'map_section' ): 
                get_template_part( 'inc/acf-content/map_section' );
            elseif( get_row_layout() == 'tab_section' ): 
                get_template_part( 'inc/acf-content/tab_section' );
            endif;
        endwhile;
    endif;
endif;

get_footer();