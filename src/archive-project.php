<?php
/**
 *
 * @package WordPress
 * @subpackage OazysDah
 * @since 1.0
 * @version 1.0
 */
get_header(); 

    get_template_part( 'inc/acf-content/archive-project/banner' ); ?>

    <section class="padding">
        <div class="container">
            <?php if ( have_posts() ) : ?>
            <div class="row">
                <?php while ( have_posts() ) :
                    the_post(); 
                    get_template_part( 'template-parts/project/content' );
                endwhile; ?>
            </div>
            <?php endif; ?>
        </div>
    </section>

<?php get_template_part( 'inc/acf-content/archive-project/form' );

get_footer();