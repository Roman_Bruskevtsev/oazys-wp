<?php
/**
 *
 * @package WordPress
 * @subpackage OazysDah
 * @since 1.0
 * @version 1.0
 */
get_header(); 

	$image = (get_the_post_thumbnail_url( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID() ).')"' : ''; 
	$gallery = get_field('gallery');
	?>
	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="col nopadding">
					<div class="products__banner"<?php echo $image; ?>>
						<span class="arrow"></span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="padding">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section__title text-center" data-aos="fade-up">
						<h2><?php the_title(); ?></h2>
						<?php if( get_field('subtitle') ) { ?>
							<p><?php the_field('subtitle'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php 
			$i = 0;
			if( $gallery ) { ?>
			<div class="row">
			<?php foreach ($gallery as $image) { 
				if($i == 0 || $i % 3 == 0 ) { 
					?>
					<div class="col-sm-12">
						<div class="project__image" data-aos="fade-up">
							<img src="<?php echo $image['sizes']['large']; ?>" data-src="<?php echo $image['url']; ?>" class="lazy" alt="<?php echo $image['title']; ?>">
						</div>
					</div>
				<?php } else { ?>
					<div class="col-sm-6">
						<div class="project__image" data-aos="fade-up">
							<img src="<?php echo $image['sizes']['medium']; ?>" data-src="<?php echo $image['url']; ?>" class="lazy" alt="<?php echo $image['title']; ?>">
						</div>
					</div>
				<?php } ?>
			<?php $i++; } ?>
			</div>
			<?php } ?>
		</div>
	</section>
<?php get_footer(); ?>

