<?php
/**
 *
 * @package WordPress
 * @subpackage OazysDah
 * @since 1.0
 * @version 1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>
<body <?php body_class('load');?>>
    <?php wp_body_open(); ?>
    <div class="preloader__wrapper">
        <div id="loading-center-absolute">
        <div class="object" id="object_one"></div>
        <div class="object" id="object_two"></div>
        <div class="object" id="object_three"></div>
        <div class="object" id="object_four"></div>
        <div class="object" id="object_five"></div>
        <div class="object" id="object_six"></div>
        <div class="object" id="object_seven"></div>
        <div class="object" id="object_eight"></div>
        <div class="object" id="object_nine"></div>
        </div>
    </div>
   
    <?php $class = get_field('notice', 'option') ? ' class="has__notice"' : ''; ?>
    <header<?php echo $class; ?>>
     <?php if( get_field('notice', 'option') ) { ?>
        <div class="notice">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <p><?php the_field('notice', 'option'); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="navigation">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="left__bar float-left">
                            <?php if( has_nav_menu('category') ) { ?>
                            <div class="menu__btn category__nav float-left">
                                <div class="icon">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                <span><?php _e('Products', 'oazys'); ?></span>
                            </div>
                            <?php } 
                            if( get_field('currency', 'option') ){ ?>
                            <div class="currency float-left">
                                <span class="label"><?php _e('EUR', 'oazys'); ?>:</span>
                                <span class="value"><?php the_field('currency', 'option'); ?></span>
                            </div>
                            <?php } ?>
                        </div>
                        <?php if( get_field('logo', 'option') ) { 
                        $logo = get_field('logo', 'option'); ?>
                        <a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        </a>
                        <?php } ?>
                        <div class="right__bar float-right">
                            <?php if( get_field('phone_1', 'option') || get_field('phone_2', 'option') ) { ?>
                            <div class="phone__block float-left">
                                <?php if( get_field('phone_1', 'option') ) { ?><a href="tel:<?php the_field('phone_1', 'option'); ?>"><?php the_field('phone_1', 'option'); ?></a><?php } ?>
                                <?php if( get_field('phone_2', 'option') ) { ?><a href="tel:<?php the_field('phone_2', 'option'); ?>"><?php the_field('phone_2', 'option'); ?></a><?php } ?>
                            </div>
                            <div class="phone__btn float-left"></div>
                            <?php } echo oazys_language_switcher(); ?>
                            <div class="cart__block float-left"><span class="cart__amount"></span></div>
                            <?php if( has_nav_menu('main') ) { ?>
                                <div class="menu__btn main__nav float-left"><span></span><span></span><span></span></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="menu__wrapper"></div>
    <?php if( has_nav_menu('category') ) { ?>
    <div class="category__menu menu__block left">
        <div class="top__nav">
            <?php echo oazys_language_switcher(); ?>
            <span class="close__menu"></span>
        </div>
        <?php wp_nav_menu( array(
            'theme_location'        => 'category',
            'container'             => 'nav',
            'container_class'       => 'category__nav'
        ) ); ?>
    </div>
    <?php } ?>
    <?php if( has_nav_menu('main') ) { ?>
    <div class="main__menu menu__block right">
        <div class="top__nav">
            <?php echo oazys_language_switcher(); ?>
            <span class="close__menu"></span>
        </div>
        <?php wp_nav_menu( array(
            'theme_location'        => 'main',
            'container'             => 'nav',
            'container_class'       => 'main__nav'
        ) ); ?>
    </div>
    <?php } 
    if( get_field('phone_1', 'option') || get_field('phone_2', 'option') ) { ?>
        <div class="phone__popup menu__block right">
            <div class="top__nav">
                <span class="close__menu"></span>
            </div>
            <div class="phones">
                <?php if( get_field('phone_text_1', 'option') ) { ?><h5><?php the_field('phone_text_1', 'option'); ?></h5><?php } ?>
                <?php if( get_field('phone_1', 'option') ) { ?><a href="tel:<?php the_field('phone_1', 'option'); ?>"><?php the_field('phone_1', 'option'); ?></a><?php } ?>
                <?php if( get_field('phone_text_2', 'option') ) { ?><h5><?php the_field('phone_text_2', 'option'); ?></h5><?php } ?>
                <?php if( get_field('phone_2', 'option') ) { ?><a href="tel:<?php the_field('phone_2', 'option'); ?>"><?php the_field('phone_2', 'option'); ?></a><?php } ?>    
            </div>
        </div>
    <?php } ?>
    <main>