<?php 
$image = get_field('contact_image', 'option');
if( get_field('show_contact_section', 'option') ) { ?>
<section class="contact__section">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-lg-5 nopadding">
				<?php if( $image ) { ?>
				<div class="content__image">
					<img src="<?php echo $image['sizes']['medium']; ?>" data-src="<?php echo $image['url']; ?>" class="lazy" alt="<?php echo $image['title']; ?>">
				</div>
				<?php } ?>
			</div>
			<div class="col-lg-5 contact__column nopadding">
				<div class="content__text">
					<div class="section__title text-left" data-aos="fade-up">
						<?php if( get_field('contact_title', 'option') ) { ?>
							<h2><?php the_field('contact_title', 'option'); ?></h2>
						<?php } ?>
						<?php if( get_field('contact_subtitle', 'option') ) { ?>
							<p><?php the_field('contact_subtitle', 'option'); ?></p>
						<?php } ?>
					</div>
					<?php if(get_field('contact_form_shortcode', 'option')){ ?>
					<div class="form__block" data-aos="fade-up">
						<?php echo do_shortcode(get_field('contact_form_shortcode', 'option')); ?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>