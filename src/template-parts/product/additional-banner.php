<?php 
if( get_field('additional_banner') ) { 
$image = get_field('additional_banner');
?>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col nopadding">
				<div class="additional__banner">
					<img src="<?php echo $image['sizes']['medium']; ?>" data-src="<?php echo $image['url']; ?>" class="lazy" alt="<?php echo $image['title']; ?>">
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>