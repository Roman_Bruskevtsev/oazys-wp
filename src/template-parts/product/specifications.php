<?php 
$gallery = get_field('gallery');
$post_thumb = get_the_post_thumbnail_url( get_the_ID() );
$table = get_field('table_sizes');
?>
<section class="padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<?php if( $gallery ) { ?>
				<div class="product__section">
					<div class="image__slider" data-aos="fade-up">
						<?php if( $gallery ) { 
							foreach ( $gallery as $image ) { ?>
							<div class="slide">
								<div class="wrappper">
									<img src="<?php echo $image['sizes']['product-category'] ?>" alt="<?php the_title(); ?>">
								</div>
							</div>
							<?php }
						} ?>
					</div>
					<div class="thumb__slider" data-aos="fade-up">
						<?php if( $gallery ) { 
							foreach ( $gallery as $image ) { ?>
							<div class="slide">
								<div class="thumb__wrapper">
									<img src="<?php echo $image['sizes']['product-category'] ?>" alt="<?php the_title(); ?>">
									<h5><?php echo $image['title']; ?></h5>
								</div>
							</div>
							<?php }
						} ?>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="col-lg-6">
				<div class="content__block" data-aos="fade-left">
					<h2><?php the_title(); ?></h2>
					<?php if( get_field('brand_logo') ) { ?>
						<div class="brand">
							<img src="<?php echo get_field('brand_logo')['url']; ?>" alt="<?php echo get_field('brand_logo')['title']; ?>">
						</div>
					<?php } ?>
					<?php if( have_rows('colors') ): ?>
					<div class="color__block">
						<span class="color__title"><?php _e('Colors:', 'oazys'); ?></span>
						<ul class="colors">
						<?php while ( have_rows('colors') ) : the_row(); 
							$color = get_sub_field('color');
							?>
							<li>
								<span style="background-color: <?php echo $color; ?>"></span>
							</li>
						<?php endwhile; ?>
						</ul>
					</div>
					<?php endif; ?>
					<div class="tab__wrapper">
						<ul class="nav">
							<li class="active"><?php _e('Technical information', 'oazys'); ?></li>
							<li><?php _e('Description', 'oazys'); ?></li>
						</ul>
						<div class="content">
							<div class="tab active">
								<?php if( have_rows('specifications') ): ?>
								<ul class="specification">
								<?php while ( have_rows('specifications') ) : the_row(); ?>
									<li>
										<span class="label"><?php the_sub_field('label'); ?></span>
										<span class="value"><?php the_sub_field('value'); ?></span>
									</li>
								<?php endwhile; ?>
								</ul>
								<?php endif; ?>
							</div>
							<div class="tab"><?php the_field('description'); ?></div>
							
						</div>
					</div>
					<?php if( $table['link'] ) { ?>
						<a href="<?php echo $table['link']; ?>" target="_blank" class="show__table"><?php echo $table['label']; ?></a>
					<?php } ?>
					<div class="btn__group">
						<button class="btn btn__simple add__to__cart" onclick="orderData.setItem(<?php echo get_the_ID(); ?>)"><span><?php _e('Add to cart', 'oazys'); ?></span></button>
						<?php if( get_field('technical_department_phone', 'option') ) { ?>
							<a href="tel:<?php echo get_field('technical_department_phone', 'option'); ?>" class="btn btn__grey"><span><?php _e('Technical department', 'oazys'); ?></span></a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>