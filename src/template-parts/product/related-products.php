<?php
$related = get_field('related');
$posts = $related['choose_products'];
if( $related['title'] || $posts ){ ?>
<section class="padding">
	<div class="container">
		<?php if( $related['title'] ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="section__title text-center nomargin" data-aos="fade-up">
				<?php if( $related['title'] ) { ?>
					<h2><?php echo $related['title']; ?></h2>
				<?php } ?>
				</div>
			</div>	
		</div>
		<?php } 

		if( $posts ) { 
			$args = array(
				'post__in'		=>	$posts,
				'post_type' 	=> 'product',
				'orderby'		=> 'post__in',
				'post_status'	=> 'publish',
				'posts_per_page'=> 3
			);
			$query  = new WP_Query($args);
			if( $query->have_posts() ) { ?>
				<div class="row">
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<div class="col-lg-4">
							<?php get_template_part( 'template-parts/product/content', 'accessories' ); ?>
						</div>
					<?php endwhile; ?>
				</div>
			<?php } wp_reset_postdata(); ?>
		<?php }
		if( $related['button_link'] ){ ?>
		<div class="row">
			<div class="col-lg-12 text-center btn__row">
				<a class="btn btn__simple" data-aos="fade-up" href="<?php echo $related['button_link']; ?>"><span><?php echo $related['button_label']; ?></span></a>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
<?php } ?>