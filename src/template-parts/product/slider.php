<?php 
if( get_field('slider') ) { 
$slider = get_field('slider');
?>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col nopadding">
				<div class="additional__slider">
				<?php foreach ($slider as $image) { 
					$image = ' style="background-image: url('.$image['url'].')"'; ?>
					<div class="slide"<?php echo $image; ?>></div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>