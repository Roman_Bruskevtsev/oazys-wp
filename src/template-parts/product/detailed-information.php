<?php 
$content = get_field('detailed_information');
if( $content['image'] || $content['title'] || $content['short_description'] || $content['full_description'] ){
?>
<section class="padding">
	<div class="container-fluid">
		<div class="row">
			<?php
			$images = $content['image'];
			if( $content['image'] ) { ?>
			<div class="col-lg-6 nopadding">
				<div class="content__image" data-aos="fade-right">
				<?php foreach ($images as $image) { 
					$background = $image ? ' style="background-image: url('.$image['url'].');"' : ''; ?>
					<div class="slide"<?php echo $background; ?>></div>
				<?php } ?>
				</div>
			</div>
			<?php } ?>
			<div class="col-lg-6">
				<div class="product__details" data-aos="fade-left">
					<div class="text text-left">
						<?php if( $content['title'] ) { ?>
							<h2><?php echo $content['title']; ?></h2>
						<?php } ?>
						<?php if( $content['short_description'] ) { ?>
							<div class="short__description"><?php echo $content['short_description']; ?></div>
							<?php if( $content['full_description'] ) { ?><a href="#" class="show__details"><?php _e('Show more', 'oazys'); ?></a><?php } ?>
						<?php } ?>
						<?php if( $content['full_description'] ) { ?>
							<div class="full_description hide"><?php echo $content['full_description']; ?></div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>