<?php 
$gallery = get_field('gallery');
$post_thumb = get_the_post_thumbnail_url( get_the_ID() );
?>
<div class="col-md-6 col-lg-4">
	<a class="product__block" href="<?php the_permalink(); ?>">
		<div class="image">
		<?php if( $post_thumb ) { ?>
			<img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'product-category'); ?>" alt="<?php the_title(); ?>">
		<?php } ?>
		</div>
		<div class="title">
			<h5><?php the_title(); ?></h5>
		</div>
	</a>
</div>