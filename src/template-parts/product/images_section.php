<?php 
$images = get_field('images_section');
if( $images ){ ?>
<section class="padding">
	<div class="container">
		<div class="row">
			<?php if( $images['image_1'] ) { ?>
			<div class="col-lg-4">
				<div class="image__block" data-aos="fade-up">
					<img src="<?php echo $images['image_1']['url']; ?>" title="<?php echo $images['image_1']['title']; ?>">
				</div>
			</div>
			<?php } ?>
			<?php if( $images['image_2'] ) { ?>
			<div class="col-lg-4">
				<div class="image__block" data-aos="fade-up">
					<img src="<?php echo $images['image_2']['url']; ?>" title="<?php echo $images['image_2']['title']; ?>">
				</div>
			</div>
			<?php } ?>
			<?php if( $images['image_3'] ) { ?>
			<div class="col-lg-4">
				<div class="image__block" data-aos="fade-up">
					<img src="<?php echo $images['image_3']['url']; ?>" title="<?php echo $images['image_2']['title']; ?>">
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
<?php } ?>