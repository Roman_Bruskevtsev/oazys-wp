<?php 
if( get_field('banner_image') ) { 
$image = (get_field('banner_image')) ? ' style="background-image: url('.get_field('banner_image').')"' : '';
?>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col nopadding">
				<div class="products__banner"<?php echo $image; ?>>
					<span class="arrow"></span>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>