<?php 
$post_thumb = get_the_post_thumbnail_url( get_the_ID() );
?>
<div class="col-sm-6">
	<a class="project__block" href="<?php the_permalink(); ?>" data-aos="fade-up">
		<div class="image">
		<?php if( $post_thumb ) { ?>
			<img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'project-small'); ?>" alt="<?php the_title(); ?>" data-src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'project-thumbnail'); ?>" class="lazy">
		<?php } ?>
		</div>
		<div class="content">
			<div class="title">
				<h4><?php the_title(); ?></h4>
				<?php if( get_field('subtitle') ) { ?><p><?php the_field('subtitle'); ?></p><?php } ?>
			</div>
		</div>
	</a>
</div>