<div class="popup__wrapper">
	<div class="popup">
		<div class="close__popup"></div>
		<div class="title text-center">
			<h2><?php _e('Ordering', 'oazys'); ?></h2>
		</div>
		<div class="body">
			<h4><?php _e('Your order', 'oazys'); ?></h4>
			<ul class="order__list"></ul>
			<form onsubmit="orderData.order(this); return false" name="order__form" class="order__form" action="<?php echo get_the_permalink(); ?>" method="post" accept-charset="utf-8">
				<?php wp_nonce_field('form__validation', 'security__field'); ?>
				<div class="field__row">
					<div class="half">
						<input class="form__field customer__name" type="text" name="name" required placeholder="<?php _e('Your name', 'oazys'); ?>"></input>
					</div>
					<div class="half">
						<input class="form__field customer__tel" type="tel" name="phone" required placeholder="<?php _e('Your phone number', 'oazys'); ?>"></input>
					</div>
				</div>
				<div class="field__row">
					<div class="full submit__row text-right">
						<div class="btn btn__grey clear__cart" onclick="orderData.clearCart();"><span><?php _e('Clear cart', 'oazys'); ?></span></div>
						<button class="btn btn__simple" type="submit"><span><?php _e('Order', 'oazys'); ?></span></button>
					</div>
				</div>
				<div class="order__status text-center"></div>
			</form>
		</div>
	</div>
</div>