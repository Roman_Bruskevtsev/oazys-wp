<?php 
$image = (get_field('protected_page_banner', 'option')) ? ' style="background-image: url('.get_field('protected_page_banner', 'option').')"' : '';
?>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col nopadding">
				<div class="fullscreen__banner"<?php echo $image; ?>>
					<div class="content text-center">
						<div class="text"><h1><?php the_title(); ?></h1></div>
						<?php echo get_the_password_form(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>