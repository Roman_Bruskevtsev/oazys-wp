<?php 
$post = get_option('page_for_posts');

$image = (get_field('blog_banner', $post)) ? ' style="background-image: url('.get_field('blog_banner', $post).')"' : '';
?>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col nopadding">
				<div class="fullscreen__banner"<?php echo $image; ?>>
					<div class="content text-center">
						<div class="text">
							<h1><?php echo get_the_title($post); ?></h1>
							<?php if( get_field('subtitle', $post) ) { ?>
								<p><?php echo get_field('subtitle', $post); ?></p>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>