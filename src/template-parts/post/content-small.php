<?php 
$post_thumb = get_the_post_thumbnail_url( get_the_ID() ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'full').')"' : '';
?>
<div class="col-md-6 col-lg-4">
	<a href="<?php the_permalink(); ?>" class="post__block small">
		<div class="image">
		<?php if( $post_thumb ) { ?>
			<span></span>
			<div class="img"<?php echo $post_thumb; ?>></div>
		<?php } ?>
		</div>
		<div class="title">
			<h5><?php the_title(); ?></h5>
		</div>
	</a>
</div>