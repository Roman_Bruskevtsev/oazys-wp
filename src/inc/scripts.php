<?php

function oazys_scripts() {
    $version = '1.0.47';

    wp_enqueue_style( 'oazys-css', get_theme_file_uri( '/assets/css/main.min.css' ), '', $version );
    wp_enqueue_style( 'oazys-style', get_stylesheet_uri() );

    wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), $version, true );
    wp_enqueue_script( 'mousewheel-smoothscroll-js', get_theme_file_uri( '/assets/js/mousewheel-smoothscroll.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'aos-js', get_theme_file_uri( '/assets/js/aos.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'slick-js', get_theme_file_uri( '/assets/js/slick.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'script-js', get_theme_file_uri( '/assets/js/main.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'cart-js', get_theme_file_uri( '/assets/js/cart.min.js' ), array( 'jquery' ), $version, true );
    if( is_single() ) wp_enqueue_script( 'sharethis-js', 'https://platform-api.sharethis.com/js/sharethis.js#property=58a3302291c4ff0011528cf0&product=custom-share-buttons', array( 'jquery' ), $version, true );
}
add_action( 'wp_enqueue_scripts', 'oazys_scripts' );

?>