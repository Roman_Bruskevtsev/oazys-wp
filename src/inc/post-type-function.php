<?php
// add_action('init', 'oazys_services_post', 0);

// function oazys_services_post() {
//   //Register taxonomy
//   $taxonomy_labels = array(
//     'name'                        => __('Services categories','oazys'),
//     'singular_name'               => __('Service category','oazys'),
//     'menu_name'                   => __('Services categories','oazys'),
//   );

//   $taxonomy_rewrite = array(
//     'slug'                  => 'services-categories',
//     'with_front'            => true,
//     'hierarchical'          => true,
//   );

//   $taxonomy_args = array(
//     'labels'              => $taxonomy_labels,
//     'hierarchical'        => true,
//     'public'              => true,
//     'show_ui'             => true,
//     'show_admin_column'   => true,
//     'show_in_nav_menus'   => true,
//     'show_tagcloud'       => true,
//     'rewrite'             => $taxonomy_rewrite,
//   );
//   register_taxonomy( 'service-category', 'service', $taxonomy_args );
  
//   //Register new post type
//   $post_labels = array(
//   'name'                => __('Services', 'oazys'),
//   'add_new'             => __('Add Service', 'oazys')
//   );

//   $post_args = array(
//   'label'               => __('Services', 'oazys'),
//   'description'         => __('Service information page', 'oazys'),
//   'labels'              => $post_labels,
//   'supports'            => array( 'title', 'thumbnail'),
//   'taxonomies'          => array( 'service-category' ),
//   'hierarchical'        => false,
//   'public'              => true,
//   'show_ui'             => true,
//   'show_in_menu'        => true,
//   'has_archive'         => true,
//   'can_export'          => true,
//   'show_in_nav_menus'   => true,
//   'publicly_queryable'  => true,
//   'exclude_from_search' => false,
//   'query_var'           => true,
//   'capability_type'     => 'post',
//   'menu_icon'           => 'dashicons-vault'
//   );
//   register_post_type( 'service', $post_args );
// }

add_action('init', 'oazys_projects_post', 0);

function oazys_projects_post() {
  //Register taxonomy
  // $taxonomy_labels = array(
  //   'name'                        => __('Projects categories','oazys'),
  //   'singular_name'               => __('Project category','oazys'),
  //   'menu_name'                   => __('Projects categories','oazys'),
  // );

  // $taxonomy_rewrite = array(
  //   'slug'                  => 'projects-categories',
  //   'with_front'            => true,
  //   'hierarchical'          => true,
  // );

  // $taxonomy_args = array(
  //   'labels'              => $taxonomy_labels,
  //   'hierarchical'        => true,
  //   'public'              => true,
  //   'show_ui'             => true,
  //   'show_admin_column'   => true,
  //   'show_in_nav_menus'   => true,
  //   'show_tagcloud'       => true,
  //   'rewrite'             => $taxonomy_rewrite,
  // );
  // register_taxonomy( 'project-category', 'project', $taxonomy_args );
  
  //Register new post type
  $post_labels = array(
  'name'                => __('Projects', 'oazys'),
  'add_new'             => __('Add Project', 'oazys')
  );

  $post_rewrite = array(
    'slug'      => 'projects'
  );

  $post_args = array(
  'label'               => __('Projects', 'oazys'),
  'description'         => __('Project information page', 'oazys'),
  'labels'              => $post_labels,
  'supports'            => array( 'title', 'thumbnail'),
  'taxonomies'          => array( '' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-admin-multisite',
  'rewrite'             => $post_rewrite
  );
  register_post_type( 'project', $post_args );
}

add_action('init', 'oazys_products_post', 0);

function oazys_products_post() {
  //Register taxonomy
  $taxonomy_labels = array(
    'name'                        => __('Products categories','oazys'),
    'singular_name'               => __('Product category','oazys'),
    'menu_name'                   => __('Products categories','oazys'),
  );

  $taxonomy_rewrite = array(
    'slug'                  => 'products-categories',
    'with_front'            => true,
    'hierarchical'          => true,
  );

  $taxonomy_args = array(
    'labels'              => $taxonomy_labels,
    'hierarchical'        => true,
    'public'              => true,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'show_in_nav_menus'   => true,
    'show_tagcloud'       => true,
    'rewrite'             => $taxonomy_rewrite,
  );
  register_taxonomy( 'product-category', 'product', $taxonomy_args );
  
  //Register new post type
  $post_labels = array(
  'name'                => __('Products', 'oazys'),
  'add_new'             => __('Add Product', 'oazys')
  );

  $post_rewrite = array(
    'slug'      => 'products'
  );

  $post_args = array(
  'label'               => __('Products', 'oazys'),
  'description'         => __('Product information page', 'oazys'),
  'labels'              => $post_labels,
  'supports'            => array( 'title', 'thumbnail'),
  'taxonomies'          => array( 'product-category' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-lightbulb',
  'rewrite'             => $post_rewrite
  );
  register_post_type( 'product', $post_args );
}