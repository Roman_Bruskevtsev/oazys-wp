<?php 
$image = (get_sub_field('image')) ? ' style="background-image: url('.get_sub_field('image').')"' : '';
?>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col nopadding">
				<div class="fullscreen__banner"<?php echo $image; ?>>
					<div class="content">
						<?php if( get_sub_field('content') ) { ?>
							<div class="text"><?php the_sub_field('content'); ?></div>
						<?php } ?>
						<?php if( get_sub_field('button') && get_sub_field('button_link') ) { ?>
							<div class="button__row text-center">
								<a href="<?php the_sub_field('button_link'); ?>" class="btn btn__white"><span><?php the_sub_field('button_label'); ?></span></a>
							</div>
						<?php } ?>
					</div>
					<span class="arrow"></span>
				</div>
			</div>
		</div>
	</div>
</section>
