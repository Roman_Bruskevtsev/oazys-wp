<?php
$categories = get_sub_field('choose_categories_to_show');
?>
<section class="padding">
	<div class="container">
		<?php if( get_sub_field('title') || get_sub_field('subtitle') ) { ?>
		<div class="row justify-content-md-center">
			<div class="col-lg-8">
				<div class="section__title text-center">
					<?php if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
					<?php if( get_sub_field('subtitle') ) { ?>
						<p><?php the_sub_field('subtitle'); ?></p>
					<?php } ?>
				</div>
			</div>	
		</div>
		<?php }
		if($categories) { ?>
		<div class="row">
			<?php foreach ($categories as $category) { 
				$thumbnail = get_field('thumbnail', $category->taxonomy.'_'.$category->term_id);
				$background = $thumbnail ? ' style="background-image: url('.$thumbnail['url'].')"' : '';
				?>
				<div class="col-lg-4 col-md-6">
					<a class="product__category" href="<?php echo get_term_link($category->term_id, $category->taxonomy); ?>" data-aos="fade-up" data-aos-duration="5000">
						<div class="title text-center">
							<h5><?php echo $category->name; ?></h5>
						</div>
						<div class="thumbnail">
							<span></span>
							<div class="img"<?php echo $background; ?>></div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>
		<?php }
		if(get_sub_field('button_link')){ ?>
		<div class="row">
			<div class="col-lg-12 text-center btn__row">
				<a class="btn btn__simple" data-aos="fade-up" href="<?php the_sub_field('button_link'); ?>"><span><?php the_sub_field('button_label'); ?></span></a>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
