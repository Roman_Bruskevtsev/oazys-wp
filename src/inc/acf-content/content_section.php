<section class="padding background">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-lg-10">
				<div class="section__title text-center" data-aos="fade-up">
					<?php if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
					<?php if( get_sub_field('text') ) { ?>
						<p><?php the_sub_field('text'); ?></p>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>