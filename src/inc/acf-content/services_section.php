<section class="padding">
	<div class="container">
		<?php if( get_sub_field('title') || get_sub_field('subtitle') ) { ?>
		<div class="row justify-content-md-center">
			<div class="col-lg-8">
				<div class="section__title text-center" data-aos="fade-up">
					<?php if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
					<?php if( get_sub_field('subtitle') ) { ?>
						<p><?php the_sub_field('subtitle'); ?></p>
					<?php } ?>
				</div>
			</div>	
		</div>
		<?php } 
		if( get_sub_field('services_title') ) { ?>
		<div class="row">
			<div class="col service__title text-center" data-aos="fade-up">
				<h4><?php the_sub_field('services_title'); ?></h4>
			</div>
		</div>
		<?php } ?>
		<?php if( have_rows('services') ): ?>
		<div class="row">
			<?php while ( have_rows('services') ) : the_row(); 
				$image = get_sub_field('image');
				$link = get_sub_field('link');
				?>
				<div class="col-md-6 col-lg-3">
					<div class="service__block" data-aos="fade-up">
						<?php if( $link ){ ?>
						<a class="image" href="<?php echo $link['url']; ?>">
							<h6><?php echo $link['title']; ?></h6>
							<?php if( $image ) { ?>
								<img src="<?php echo $image['sizes']['medium']; ?>" data-src="<?php echo $image['url']; ?>" class="lazy" alt="<?php echo $image['title']; ?>">
							<?php } ?>
						</a>
						<?php } else { ?>
							<div class="image">
								<?php if( $image ) { ?>
									<img src="<?php echo $image['sizes']['medium']; ?>" data-src="<?php echo $image['url']; ?>" class="lazy" alt="<?php echo $image['title']; ?>">
								<?php } ?>
							</div>
						<?php } ?>
						<div class="title">
							<?php if( get_sub_field('name') ) { ?><h5><?php echo get_sub_field('name'); ?></h5><?php } ?>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
</section>
