<?php 
$map = get_sub_field('map');
$content = get_sub_field('content');

?>
<section class="padding">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-7 nopadding">
				<?php if($map) { ?>
				<div class="map__block">
			        <div class="map__wrapper" id="map-canvas"
			             data-lat="<?php echo $map['latitude']; ?>"
			             data-lng="<?php echo $map['longitude']; ?>"
			             data-zoom="<?php echo $map['zoom']; ?>"
			             data-marker="<?php echo $map['marker']; ?>">
			        </div>
			    </div>
				<?php } ?>
			</div>
			<div class="col-lg-5 nopadding" data-aos="fade-up">
				<?php if($content) { ?>
				<div class="content__text">
					<div class="section__title text-left">
						<?php if( $content['title'] ) { ?>
							<h2><?php echo $content['title']; ?></h2>
						<?php } ?>
						<?php if( $content['text'] ) { ?>
							<div class="text__field">
								<?php echo $content['text']; ?>
							</div>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>