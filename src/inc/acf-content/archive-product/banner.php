<?php if( get_field('banner_image', 'option') || get_field('banner_content', 'option') ) { 
$image = (get_field('banner_image', 'option')) ? ' style="background-image: url('.get_field('banner_image', 'option').')"' : '';
$content = get_field('banner_content', 'option');
?>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col nopadding">
				<div class="products__banner"<?php echo $image; ?>>
					<?php if( $content ) { ?>
						<div class="content"><?php echo $content; ?></div>
					<?php } ?>
					<span class="arrow"></span>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>