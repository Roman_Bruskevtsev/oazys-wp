<?php 
$image = get_sub_field('image');
?>
<section class="padding">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-xl-7 nopadding">
				<div class="content__image" data-aos="fade-right">
					<img src="<?php echo $image['sizes']['medium']; ?>" data-src="<?php echo $image['url']; ?>" class="lazy" alt="<?php echo $image['title']; ?>">
				</div>
			</div>
			<div class="col-lg-6 col-xl-4 nopadding" data-aos="fade-up">
				<div class="content__text">
					<div class="section__title text-left">
						<?php if( get_sub_field('title') ) { ?>
							<h2><?php the_sub_field('title'); ?></h2>
						<?php } ?>
						<?php if( get_sub_field('text') ) { ?>
							<div class="text__field">
								<?php the_sub_field('text'); ?>
							</div>
						<?php } ?>
					</div>
					<?php if(get_sub_field('button_link')){ ?>
					<div class="text-left btn__row">
						<a class="btn btn__simple" href="<?php the_sub_field('button_link'); ?>"><span><?php the_sub_field('button_label'); ?></span></a>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>