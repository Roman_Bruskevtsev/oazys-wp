<?php 
$image = get_sub_field('image');
$background = $image ? ' style="background-image: url('.$image['url'].');"' : '';
?>
<section class="contact__section">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-lg-9 contact__column nopadding">
				<div class="contact__wrapper"<?php echo $background; ?>>
					<div class="content__text">
						<div class="section__title text-left" data-aos="fade-up">
							<?php if( get_sub_field('title') ) { ?>
								<h2><?php the_sub_field('title'); ?></h2>
							<?php } ?>
							<?php if( get_sub_field('text') ) { ?>
								<p><?php the_sub_field('text'); ?></p>
							<?php } ?>
						</div>
						<?php if(get_sub_field('form_shortcode')){ ?>
						<div class="form__block" data-aos="fade-up">
							<?php echo do_shortcode(get_sub_field('form_shortcode')); ?>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>