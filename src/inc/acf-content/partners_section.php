<section>
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-lg-10">
				<div class="section__title text-center">
					<?php if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
					<?php if( get_sub_field('text') ) { ?>
						<p><?php the_sub_field('text'); ?></p>
					<?php } ?>
				</div>
				<?php if( have_rows('partners') ): ?>
				<div class="partners__slider">
					<?php while ( have_rows('partners') ) : the_row(); 
						$image = get_sub_field('logo');
						$link = get_sub_field('link');
					?>
					<div class="slide">
						<?php if( $image ) { ?>
						<?php if( $link ) { ?><a href="<?php echo $link; ?>" target="_blank"><span><?php } else { ?><div class="partner text-center"><?php } ?>
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
						<?php if( $link ) { ?></span></a><?php } else { ?></div><?php } ?>
						<?php } ?>
					</div>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
			</div>	
		</div>
	</div>
</section>
