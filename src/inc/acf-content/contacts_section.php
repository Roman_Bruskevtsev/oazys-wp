<section class="padding">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-lg-10">
				<div class="section__title nomargin text-center" data-aos="fade-up">
					<?php if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php if( have_rows('contacts') ): ?>
		<div class="row justify-content-md-center">
			<div class="col-lg-10">
				<div class="row justify-content-md-center">
				<?php while ( have_rows('contacts') ) : the_row(); 
					$image = get_sub_field('avatar');
					?>
					<div class="col-lg-4">
						<div class="contact__block" data-aos="fade-up">
							<div class="avatar">
							<?php if( $image ){ ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
							<?php } ?>
							</div>
							<div class="content">
							<?php if( get_sub_field('name') ) { ?>
								<h4><?php the_sub_field('name'); ?></h4>
							<?php } ?>
							<?php if( get_sub_field('name') ) { ?>
								<a href="tel:<?php the_sub_field('phone'); ?>"><?php the_sub_field('phone'); ?></a>
							<?php } ?>
							<?php if( get_sub_field('position') ) { ?><span><?php the_sub_field('position'); ?></span><?php } ?>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section>