<section>
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-lg-10">
				<div class="section__title small__margin text-center">
					<?php if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
				</div>
				<?php if( have_rows('testimonials') ): ?>
				<div class="testimonial__slider">
					<?php while ( have_rows('testimonials') ) : the_row(); ?>
					<div class="slide">
						<?php if( get_sub_field('text') ) { ?>
						<div class="content text-center">
							<p><?php the_sub_field('text'); ?></p>
							<span class="name"><?php the_sub_field('name'); ?></span>
						</div>
						<?php } ?>
					</div>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
			</div>	
		</div>
	</div>
</section>
