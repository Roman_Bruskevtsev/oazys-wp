<?php if( get_field('project_banner', 'option') || get_field('project_banner', 'option') ) { 
$image = (get_field('project_banner', 'option')) ? ' style="background-image: url('.get_field('project_banner', 'option').')"' : '';
$content = get_field('projects_content', 'option');
?>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col nopadding">
				<div class="products__banner"<?php echo $image; ?>>
					<?php if( $content ) { ?>
						<div class="content"><?php echo $content; ?></div>
					<?php } ?>
					<span class="arrow"></span>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>