<?php if( get_field('form_title', 'option') || get_field('form_shortcode', 'option') ){ ?>
<section class="contact__section">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-lg-8 form__section">
				<div class="section__title text-center" data-aos="fade-up">
					<?php if( get_field('form_title', 'option') ) { ?>
						<h2><?php the_field('form_title', 'option'); ?></h2>
					<?php } ?>
				</div>
				<?php if(get_field('form_shortcode', 'option')){ ?>
				<div class="form__block" data-aos="fade-up">
					<?php echo do_shortcode(get_field('form_shortcode', 'option')); ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<?php } ?>