<?php 
$image = get_sub_field('image');
if( $image ) { ?>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col nopadding">
				<div class="fullscreen__image">
					<img src="<?php echo $image['sizes']['medium']; ?>" data-src="<?php echo $image['url']; ?>" class="lazy" alt="<?php echo $image['title']; ?>">
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>