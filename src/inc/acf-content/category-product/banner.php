<?php 
$term_info = get_queried_object();
$term_tax = $term_info->taxonomy;
$term_id = $term_info->term_id;

if( get_field('thumbnail', $term_tax.'_'.$term_id) ) { 
$image = (get_field('thumbnail', $term_tax.'_'.$term_id)) ? ' style="background-image: url('.get_field('thumbnail', $term_tax.'_'.$term_id)['url'].')"' : '';

?>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col nopadding">
				<div class="products__banner"<?php echo $image; ?>>
						<div class="content text-center">
							<h2><?php single_term_title(); ?></h2>
							<?php if( get_field('subtitle', $term_tax.'_'.$term_id) ) { ?>
							<p><?php echo get_field('subtitle', $term_tax.'_'.$term_id); ?></p>
							<?php } ?>
						</div>
					<span class="arrow"></span>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>