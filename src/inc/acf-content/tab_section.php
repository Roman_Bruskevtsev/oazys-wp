<?php if( have_rows('tabs') ): ?>
<section class="padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="tabs__section">
					<div class="tabs__title">
						<?php 
						$i = 1;
						while ( have_rows('tabs') ) : the_row(); 
							$active = ($i == 1) ? ' active' : ''; ?>
							<div class="title tab__<?php echo $i; echo $active; ?>">
								<?php the_sub_field('tab_title'); ?>
							</div>
						<?php $i++; 
						endwhile; ?>
					</div>
					<div class="tabs__content">
						<?php 
						$i = 1;
						while ( have_rows('tabs') ) : the_row();
						$active = ($i == 1) ? ' active' : ''; ?>
							<div class="tab tab__<?php echo $i; echo $active; ?>">
								<?php if( have_rows('block') ): ?>
									<?php while ( have_rows('block') ) : the_row(); ?>
										<div class="tab__block">
											<?php if( get_sub_field('title') ) { ?>
											<div class="title"><h4><?php the_sub_field('title'); ?></h4></div>
											<?php } ?>
											<?php if( have_rows('rows') ): ?>
												<?php while ( have_rows('rows') ) : the_row(); ?>
													<?php if( get_sub_field('link') ) { ?>
													<a href="<?php the_sub_field('link'); ?>" target="_blank"><?php the_sub_field('label'); ?></a>
													<?php } ?>
												<?php endwhile; ?>
											<?php endif; ?>
										</div>
									<?php endwhile; ?>
								<?php endif; ?>
							</div>
						<?php $i++; 
						endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>