<?php
/**
 *
 * @package WordPress
 * @subpackage OazysDah
 * @since 1.0
 */

/*ACF Import*/
require get_template_directory() . '/inc/acf-import.php';

/*Scripts*/
require get_template_directory() . '/inc/scripts.php';

/*Post Type and Taxonomy*/
require get_template_directory() . '/inc/post-type-function.php';

/*Theme setup*/
function oazys_setup() {
    load_theme_textdomain( 'oazys' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );

    add_image_size( 'product-category', 445, 376, true );
    add_image_size( 'project-small', 339, 200, true );
    add_image_size( 'project-thumbnail', 678, 400, true );
    add_image_size( 'post-small', 446, 400, true );
    add_image_size( 'post-normal', 678, 424, true );
    // add_image_size( 'portfolio-thumbnail', 650, 330, true );
    // add_image_size( 'portfolio-medium', 825, 464, true );

    register_nav_menus( array(
        'main'          => __( 'Main Menu', 'oazys' ),
        'category'      => __( 'Category Menu', 'oazys' ),
        'products'      => __( 'Products Menu', 'oazys' )
    ) );
}
add_action( 'after_setup_theme', 'oazys_setup' );

/*Theme settings*/
if( function_exists('acf_add_options_page') ) {
    $general = acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'redirect'      => false,
        'capability'    => 'edit_posts',
        'menu_slug'     => 'theme-settings',
    ));
}

/*SVG support*/
function oazys_svg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'oazys_svg_types', 99);

/*JS variables*/
function oazys_js_variables() {
?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
<?php
}
add_action('wp_footer','oazys_js_variables');

/*Disable Gutenberg editor*/
add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);

/*Language Switcher*/
function oazys_language_switcher(){
	if (!function_exists( 'pll_the_languages')) return;
	$output = '';
	$languages = pll_the_languages(array(
	    'display_names_as'       => 'slug',
	    'hide_if_no_translation' => 1,
	    'raw'                    => true
	));

	$output .= '<div class="language__switcher">';
		$output .= '<ul>';
		foreach ($languages as $lang) {
			if($lang['current_lang']) {
				$output .= '<li class="current">';
			} else {
				$output .= '<li>';
			}
				$output .= '<a href="'.$lang['url'].'">'.$lang['name'].'</a>';
			$output .= '</li>';
		}
		$output .= '</ul>';
	$output .= '</div>';
	
	return $output;
}

/*Register sidebar*/
function oazys_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Footer 1', 'oazys' ),
        'id'            => 'footer-1',
        'description'   => __( 'Add widgets here to appear in your footer.', 'oazys' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h5>',
        'after_title'   => '</h5>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 2', 'oazys' ),
        'id'            => 'footer-2',
        'description'   => __( 'Add widgets here to appear in your footer.', 'oazys' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h5>',
        'after_title'   => '</h5>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 3', 'oazys' ),
        'id'            => 'footer-3',
        'description'   => __( 'Add widgets here to appear in your footer.', 'oazys' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h5>',
        'after_title'   => '</h5>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 4', 'oazys' ),
        'id'            => 'footer-4',
        'description'   => __( 'Add widgets here to appear in your footer.', 'oazys' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h>',
        'after_title'   => '</h5>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 5', 'oazys' ),
        'id'            => 'footer-5',
        'description'   => __( 'Add widgets here to appear in your footer.', 'oazys' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h5>',
        'after_title'   => '</h5>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Products categories widget', 'oazys' ),
        'id'            => 'products-widget',
        'description'   => __( 'Add widgets here to appear in products categories pages.', 'oazys' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h5>',
        'after_title'   => '</h5>',
    ) );
}
add_action( 'widgets_init', 'oazys_widgets_init' );

function oazys_no_limit_projects( $query ) {
    if( ! is_admin()
        && $query->is_post_type_archive( 'project' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', -1 );
    }
}
add_action( 'pre_get_posts', 'oazys_no_limit_projects' );

add_filter( 'the_password_form', 'custom_password_form' );
function custom_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );

    $form = '<form class="protected__form" action="'.esc_url(site_url('wp-login.php?action=postpass', 'login_post')).'" method="post">';
        $form .= '<div class="password__field">';
            $form .= '<input name="post_password" id="' . $label . '" type="password" class="form__field" placeholder="'.__('Enter the password', 'oazys').'" />';
            $form .= '<button type="submit" class="submit__btn"></button>';
        $form .= '</div>';
    $form .= '</form>';
    return $form;
}

// add_filter('acf/settings/show_admin', '__return_false');

/*Cart*/
if (!function_exists('oazys_cart_items')) {
    add_action( 'wp_ajax_oazys_cart_items', 'oazys_cart_items' );
    add_action( 'wp_ajax_nopriv_oazys_cart_items', 'oazys_cart_items' );
    function oazys_cart_items() {
        $items = json_decode($_POST['data']);

        if( $items ){
            $args = array(
                'post__in'      =>  $items,
                'post_type'     => 'product',
                'orderby'       => 'post__in',
                'post_status'   => 'publish',
                'posts_per_page'=> -1
            );
            $query  = new WP_Query($args);

            if( $query->have_posts() ) :
                while ( $query->have_posts() ) : $query->the_post(); 
                    $post_thumb = get_the_post_thumbnail_url( get_the_ID() ); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <div class="thumbnail">
                            <?php if( $post_thumb ) { ?>
                                <img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'thumbnail'); ?>" alt="<?php the_title(); ?>">
                            <?php } ?>
                            </div>
                            <div class="content">
                                <h5><?php the_title(); ?></h5>
                            </div>
                        </a>
                    </li>
                <?php endwhile;
            endif;
            wp_reset_postdata();
        } else { ?>
            <li class="empty"><h5><?php _e('Cart is empty', 'oazys'); ?></h5></li>
        <?php }
        
        wp_die();
    }
}

// Function to change sender name
function  oazys_sender_name( $original_email_from ) {
    return get_bloginfo('name');
}
add_filter( 'wp_mail_from_name', 'oazys_sender_name' );

/*Order*/
if (!function_exists('oazys_cart_order')) {
    add_action( 'wp_ajax_oazys_cart_order', 'oazys_cart_order' );
    add_action( 'wp_ajax_nopriv_oazys_cart_order', 'oazys_cart_order' );
    function oazys_cart_order() {

        if(  wp_verify_nonce( $_POST['security__field'], 'form__validation' ) ) {
            $items = json_decode($_POST['items']);
            $name = esc_html($_POST['name']);
            $tel = esc_html($_POST['tel']);

            if( !$items ){ ?>
                <div class="status fail"><?php _e('Please add the product to the cart.', 'oazys'); ?></div>
            <?php } else { ?>
                <div class="status success"><?php _e('We have received your order, our managers will contact you as soon as possible to clarify the details.', 'oazys'); ?></div>
                <?php 
                $cart_items = '';

                $args = array(
                    'post__in'      =>  $items,
                    'post_type'     => 'product',
                    'orderby'       => 'post__in',
                    'post_status'   => 'publish',
                    'posts_per_page'=> -1
                );
                $query  = new WP_Query($args);

                if( $query->have_posts() ) :
                    while ( $query->have_posts() ) : $query->the_post(); 

                        $cart_items .= '
                            <tr>
                                <td width="35%" style="padding: 5px 5px 5px 30px; text-align:left;">
                                    <img src="'.get_the_post_thumbnail_url( get_the_ID(), 'thumbnail').'" alt="'.get_the_title().'">
                                </td>
                                <td width="65%" colspan="5" style="padding: 5px 30px 5px 5px; font-family: Arial;">
                                    <a href="'.get_the_permalink().'" style="margin-top: 10px; font-size: 16px; font-weight: 400; line-height: 21px; color: #122947;"><h5>'.get_the_title().'</h5></a>
                                </td>
                            </tr>';
                    endwhile;
                endif;
                wp_reset_postdata();

                $message = '
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                <html>
                <head>
                    <meta charset="utf-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <link rel="profile" href="http://gmpg.org/xfn/11">
                </head>
                <body style="padding: 0; margin: 0;">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="position: relative; padding: 50px 50px 58px 50px; background-color: #ffffff; margin: 0;">
                        <thead>
                            <tr>
                                <th>
                                    <a href="https://oazys-dah.com.ua/" target="_blank">
                                        <img src="https://oazys-dah.com.ua/wp-content/themes/oazys/assets/images/logo.png" width="141" height="40" alt="" style="margin: 0 auto 36px;">
                                    </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <table width="600" cols="6" cellspacing="0" cellpadding="0" border="0" style="position: relative; z-index: 100; border-radius: 4px; background-color: #ffffff; border: 1px solid #DCDCDC; overflow: hidden; margin: auto;">
                                        <thead style="position: relative; z-index: 0; height: 135px;">
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            <tr style="position: relative; z-index: 0;">
                                                <td colspan="4" style="padding: 30px 30px 15px 30px;">
                                                    <h1 style="margin: 0 0 7px 0; font-family: Arial; font-size: 25px; letter-spacing: 0.5px; color: #2b2b2b; font-weight: 400;">'.__('We got new order!','oazys').'</h1>
                                                </td>
                                                <td colspan="2"></td>
                                            </tr>
                                        </thead>
                                        <tbody style="background-color: #f6f6f6;">
                                            <tr>
                                                <td colspan="6" style="padding: 20px 30px 10px; font-family: Arial; font-size: 20px; line-height: 1; letter-spacing: 0.4px; color: #2b2b2b;">'.__('Form details:','oazys').'</td>
                                            </tr>
                                            '.$cart_items.'
                                            <tr>
                                                <td colspan="6" style="padding: 10px 30px 10px; font-family: Arial; font-size: 20px; line-height: 1; letter-spacing: 0.4px; color: #2b2b2b;"></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2" style="padding: 20px 29px 0 29px;">
                                                    <p style="font-family: Arial; color: #656565; font-size: 14px; line-height: 24px; text-align: left; margin: 0;">'.__('Customer name:', 'oazys').'</p>
                                                </td>
                                                <td colspan="4" style="padding: 20px 29px 0 0;">
                                                    <p style="font-family: Arial; color: #1C1A19; font-size: 14px; line-height: 24px; text-align: left; margin: 0;">'.$name.'</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="padding: 5px 29px 30px 29px;">
                                                    <p style="font-family: Arial; color: #656565; font-size: 14px; line-height: 24px; text-align: left; margin: 0;">'.__('Customer phone:', 'oazys').'</p>
                                                </td>
                                                <td colspan="4" style="padding: 5px 29px 30px 0;">
                                                    <p style="font-family: Arial; color: #1C1A19; font-size: 14px; line-height: 24px; text-align: left; margin: 0;">'.$tel.'</p>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
                </html>';

                $headers = "Content-type: text/html; charset=utf-8 \r\n";  
                $headers .= __('From', 'oazys').': oazisdah@gmail.com' . "\r\n";
                $headers .= __('Order form', 'oazys');

                $orderSubject = __('We have a new order!', 'oazys');
                $emails = get_field('sales_emails', 'option');
                $emails = explode(',', $emails);

                wp_mail( $emails, $orderSubject, $message, $headers );
            }
            wp_die();
        }
    }
}

/*Load Products*/
if (!function_exists('oazys_load_products')) {
    add_action( 'wp_ajax_oazys_load_products', 'oazys_load_products' );
    add_action( 'wp_ajax_nopriv_oazys_load_products', 'oazys_load_products' );
    function oazys_load_products() {
        if( !isset($_POST['category_id']) ) wp_die('id_false');
        $catID = $_POST['category_id'];
        $page = $_POST['page'];

        $posts_per_page = (int) get_option('posts_per_page');
        if( isset($page) ){
            $posts_offset = $page * $posts_per_page;
        }
        $args = array(
            'posts_per_page'        => $posts_per_page,
            'orderby'               => 'date',
            'ignore_sticky_posts'   => 1,
            'post_status'           => 'publish',
            'post_type'             => 'product'
        );
        if( $catID !== '*' ){
            $args[]['tax_query'] = [
                [
                    'taxonomy' => 'product-category',
                    'field'    => 'id',
                    'terms'    => $cat_id
                ]
            ];
        }
        if( isset($posts_offset) ){
            $args['offset'] = $posts_offset;
        }
        
        $query = new WP_Query( $args );
        $max_pages = $query->max_num_pages;
        if ( $query->have_posts() ) { ?>
            <?php while ( $query->have_posts() ) { $query->the_post();

                get_template_part( 'template-parts/product/content' );
            } ?>
        <?php } wp_reset_postdata(); 
        wp_die();
    }
}

/*Load posts*/
if (!function_exists('oazys_load_posts')) {
    add_action( 'wp_ajax_oazys_load_posts', 'oazys_load_posts' );
    add_action( 'wp_ajax_nopriv_oazys_load_posts', 'oazys_load_posts' );
    function oazys_load_posts() {
        if( !isset($_POST['page']) ) wp_die('id_false');
        $page = $_POST['page'];

        $posts_per_page = (int) get_option('posts_per_page');
        if( isset($page) ){
            $posts_offset = $page * $posts_per_page;
        }
        $args = array(
            'posts_per_page'        => $posts_per_page,
            'orderby'               => 'date',
            'ignore_sticky_posts'   => 1,
            'post_status'           => 'publish',
        );

        if( isset($posts_offset) ){
            $args['offset'] = $posts_offset;
        }
        
        $query = new WP_Query( $args );
        $max_pages = $query->max_num_pages;
        if ( $query->have_posts() ) { ?>
            <?php while ( $query->have_posts() ) { $query->the_post();

                get_template_part( 'template-parts/post/content', 'nolazy' );
            } ?>
        <?php } wp_reset_postdata(); 
        wp_die();
    }
}