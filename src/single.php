<?php
/**
 *
 * @package WordPress
 * @subpackage OazysDah
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
	<section class="padding">
		<div class="container">
			<div class="row justify-content-md-center">
				<div class="col-lg-8">
					<div class="post__title text-center">
						<h1><?php the_title(); ?></h1>
						<span class="date"><?php echo get_the_date(); ?></span>
					</div>
				</div>
			</div>
			<?php while ( have_posts() ) :
				the_post();
			?>
			<div class="row justify-content-md-center">
				<div class="col-lg-6">
					<div class="post__content"><?php the_content(); ?></div>
					<div class="post__sharing">
						<h4><?php _e('Share:', 'oazys'); ?></h4>
						<ul>
							<li>
								<span class="facebook st-custom-button" data-network="facebook" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_content()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></span>
							</li>
							<li>
								<span class="twitter st-custom-button" data-network="twitter" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_content()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<?php endwhile;
		    $all_posts_args = array(
		        'posts_per_page'        => 3,
		        'orderby'               => 'rand',
		        'post_status'           => 'publish',
		        'post_type'             => 'post',
		        'post__not_in'          => array(get_the_ID())
		    );

		    $all_query = new WP_Query( $all_posts_args );

		    if ( $all_query->have_posts() ) { ?>
		    <div class="row">
            	<div class="col-lg-12">
                	<div class="section__title nomargin text-center" data-aos="fade-top">
		                <h2><?php _e('Other posts', 'oazys'); ?></h2>
		            </div>
		        </div>
		    </div>
		    <div class="row">
		        <?php while ( $all_query->have_posts() ) { $all_query->the_post(); ?>
		            <?php get_template_part( 'template-parts/post/content', 'small'); ?>
		        <?php } ?>
		    </div>
		    <?php } wp_reset_postdata();  ?>
		</div>
	</section>
<?php get_footer(); ?>