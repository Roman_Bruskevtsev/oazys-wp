<?php
/**
 *
 * @package WordPress
 * @subpackage OazysDah
 * @since 1.0
 * @version 1.0
 */
get_header();

	get_template_part( 'template-parts/blog/banner' ); ?>

	<section class="padding">
        <div class="container">
            <div class="row">
            	<?php 
                $posts_per_page = (int) get_option('posts_per_page');
                $cat_id = get_queried_object()->term_id;

                $all_posts_args = array(
                    'posts_per_page'        => $posts_per_page,
                    'orderby'               => 'date',
                    'post_status'           => 'publish'
                );

                $all_query = new WP_Query( $all_posts_args );
                $max_pages = (int) $all_query->max_num_pages;

                if ( $all_query->have_posts() ): ?>
                <div class="products__wrapper" data-page="1" data-max-page="<?php echo $max_pages; ?>" data-cat="*">
                    <div class="row">
                        <?php
                        $i = 1; 
                        while ( $all_query->have_posts() ) : $all_query->the_post();  
                            if($i == 3 || $i == 4 || $i == 5){
                            	get_template_part( 'template-parts/post/content', 'small' );
                            } else {
                            	get_template_part( 'template-parts/post/content' );
                            }
                        $i++;
                        endwhile; ?>
                    </div>  
                </div>
                <?php if ( $max_pages > 1) { ?>
                <div class="show__more text-center">
                    <button class="btn btn__simple load__posts"><span><?php _e('Show more posts', 'oazys'); ?></span></button>
                </div>
                <?php } endif; ?>
            </div>
        </div>
    </section>

<?php get_footer();