<?php
/**
 *
 * @package WordPress
 * @subpackage OazysDah
 * @since 1.0
 * @version 1.0
 */
get_header(); 

    get_template_part( 'inc/acf-content/archive-product/banner' ); ?>

    <section class="padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="sidebar__widgets desktop d-none d-lg-block">
                        <?php if ( is_active_sidebar( 'products-widget' ) ) { ?>
                            <?php dynamic_sidebar( 'products-widget' ); ?>
                        <?php } ?>
                        <!-- <?php $args = array(
                            'taxonomy'   => $term_tax,
                            'hide_empty' => true,
                            'child_of'   => $term_id
                        );
                        $term_query = new WP_Term_Query( $args ); 
                        
                        if( $term_query->terms ){ ?>
                            <div class="type__block widget">
                                <h4><?php _e('Type', 'oazys'); ?></h4>
                                <ul class="menu">
                                   <?php foreach( $term_query->terms as $term ){ ?>
                                        <li>
                                            <a href="<?php echo get_term_link($term->term_id, $term_tax); ?>"><?php echo $term->name; ?></a>
                                        </li>
                                    <?php } ?> 
                                </ul>
                            </div>
                        <?php } ?> -->
                    </div>
                    <div class="sidebar__widgets mobile d-block d-lg-none">
                        <?php if ( is_active_sidebar( 'products-widget' ) ) { ?>
                            <?php dynamic_sidebar( 'products-widget' ); ?>
                        <?php } ?>
                        <!-- <?php $args = array(
                            'taxonomy'   => $term_tax,
                            'hide_empty' => true,
                            'child_of'   => $term_id
                        );
                        $term_query = new WP_Term_Query( $args ); 
                        
                        if( $term_query->terms ){ ?>
                            <div class="type__block widget">
                                <h4><?php _e('Type', 'oazys'); ?></h4>
                                <ul class="menu">
                                   <?php foreach( $term_query->terms as $term ){ ?>
                                        <li>
                                            <a href="<?php echo get_term_link($term->term_id, $term_tax); ?>"><?php echo $term->name; ?></a>
                                        </li>
                                    <?php } ?> 
                                </ul>
                            </div>
                        <?php } ?> -->
                    </div>
                </div>
                <div class="col-lg-9">
                    <?php 
                    $posts_per_page = (int) get_option('posts_per_page');

                    $all_posts_args = array(
                        'posts_per_page'        => $posts_per_page,
                        'orderby'               => 'date',
                        'post_status'           => 'publish',
                        'post_type'             => 'product'
                    );

                    $all_query = new WP_Query( $all_posts_args );
                    $max_pages = (int) $all_query->max_num_pages;

                    if ( $all_query->have_posts() ): ?>
                    <div class="products__wrapper" data-page="1" data-max-page="<?php echo $max_pages; ?>" data-cat="*">
                        <div class="row">
                            <?php while ( $all_query->have_posts() ) : $all_query->the_post(); 
                                get_template_part( 'template-parts/product/content' );
                            endwhile; ?>
                        </div>  
                    </div>
                    <?php if ( $max_pages > 1) { ?>
                        <div class="show__more text-center">
                            <button class="btn btn__simple load__products"><span><?php _e('Show more product', 'oazys'); ?></span></button>
                        </div>
                        <?php } 
                    endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php get_template_part( 'template-parts/contact-section' );

get_footer();