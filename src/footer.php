<?php
/**
 *
 * @package WordPress
 * @subpackage OazysDah
 * @since 1.0
 * @version 1.0
 */
?>  
    </main>
    <footer>
        <div class="container">
        	<div class="row">
        		<?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
                    <div class="col-lg-2"><?php dynamic_sidebar( 'footer-1' ); ?></div>
                <?php } ?>
                <?php if ( is_active_sidebar( 'footer-2' ) ) { ?>
                    <div class="col-lg-2"><?php dynamic_sidebar( 'footer-2' ); ?></div>
                <?php } ?>
                <?php if ( is_active_sidebar( 'footer-3' ) ) { ?>
                    <div class="col-lg-3"><?php dynamic_sidebar( 'footer-3' ); ?></div>
                <?php } ?>
                <?php if ( is_active_sidebar( 'footer-4' ) ) { ?>
                    <div class="col-lg-3"><?php dynamic_sidebar( 'footer-4' ); ?></div>
                <?php } ?>
                <?php if ( is_active_sidebar( 'footer-5' ) ) { ?>
                    <div class="col-lg-2"><?php dynamic_sidebar( 'footer-5' ); ?></div>
                <?php } ?>
        	</div>
        </div>
    </footer>
    <?php 
    get_template_part( 'template-parts/cart' );
    wp_footer(); ?>
</body>
</html>