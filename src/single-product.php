<?php
/**
 *
 * @package WordPress
 * @subpackage OazysDah
 * @since 1.0
 * @version 1.0
 */
get_header(); 

	get_template_part( 'template-parts/product/banner' );

	get_template_part( 'template-parts/product/specifications' );

	get_template_part( 'template-parts/product/slider' );

	get_template_part( 'template-parts/product/images_section' );

	// get_template_part( 'template-parts/product/additional-banner' );

	get_template_part( 'template-parts/product/detailed-information' );

	get_template_part( 'template-parts/product/accessories' );

	get_template_part( 'template-parts/product/related-products' );


get_footer(); ?>