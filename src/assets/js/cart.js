(function( $ ) {
    'use strict';

    /*Cart*/
	$('.cart__block').on('click', function(){
		$('.popup__wrapper').addClass('show');
	});

	$('.close__popup').on('click', function(){
		$('.popup__wrapper').removeClass('show');
	});

} )( jQuery );

'use strict';

class oazysOrder{
    constructor(cartData){
        this.cartStorage          = 'cartStorage';
        this.orderButton          = document.querySelector('.add__to__cart');
        this.itemsAmount          = document.querySelector('.cart__amount');
        this.cartList			  = document.querySelector('.order__list');

        this.setItem();

    }

    setItem(id = ''){
    	let jsonArray, cartArray,
    		cartData = this.getItems();

    	if( id ){

    		jsonArray = JSON.stringify([id]);
    		
    		if( cartData != null ){

    			cartArray = JSON.parse(cartData);

    			if( !cartArray.includes(id) ){

    				cartArray.push(id);
    			}

    			jsonArray = JSON.stringify(cartArray);
				localStorage.setItem(this.cartStorage, jsonArray);

    		} else {
    			
    			jsonArray = JSON.stringify([id]);
    			localStorage.setItem(this.cartStorage, jsonArray);

    		}
    		

    	} else {

    		if( cartData != null && cartData ){

    			cartArray = JSON.parse(cartData);
				jsonArray = JSON.stringify(cartArray);
				localStorage.setItem(this.cartStorage, jsonArray);

    		}

    	}

    	this.setItemsAmount(jsonArray);
		this.addToCart(jsonArray);
    }

    getItems(){
    	return localStorage.getItem(this.cartStorage);
    }

    setItemsAmount(array){
    	let arrayAmount = JSON.parse(this.getItems());

    	if(arrayAmount == null){
    		this.itemsAmount.innerHTML = '';
    	} else {
    		this.itemsAmount.innerHTML = arrayAmount.length;
    	}
    }

    clearCart(){
    	localStorage.clear();

    	this.setItem();
    }

    addToCart(json = ''){
    	
	    (function( $ ) {
	        $.post( ajaxurl, {
	            'action'  : 'oazys_cart_items',
	            'data'    : json
	        })
	        .done(function(response) {
	            document.querySelector('.order__list').innerHTML = response;
	        });
	    } )( jQuery );
        
    }

    order(form){

    	let order = this.getItems(),
    		name = form.querySelector('.customer__name').value,
        	tel = form.querySelector('.customer__tel').value,
        	security = form.querySelector('input[name="security__field"]').value;

    	(function( $ ) {
	        $.post( ajaxurl, {
	            'action'  : 'oazys_cart_order',
	            'name'    : name,
	            'tel'	  : tel,
	            'security__field': security,
	            'items'	  : order
	        })
	        .done(function(response) {
	        	localStorage.clear();

    			document.querySelector('.order__list').innerHTML = '';
    			document.querySelector('.cart__amount').innerHTML = '';
	            document.querySelector('.order__status').innerHTML = response;
	        });
	    } )( jQuery );
    }
}

let orderData = new oazysOrder();