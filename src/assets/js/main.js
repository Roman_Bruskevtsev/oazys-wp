document.addEventListener("DOMContentLoaded", function() {
  let lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
  let active = false;

  const lazyLoad = function() {
    if (active === false) {
      active = true;

      setTimeout(function() {
        lazyImages.forEach(function(lazyImage) {
          if ((lazyImage.getBoundingClientRect().top <= window.innerHeight && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== "none") {
            lazyImage.src = lazyImage.dataset.src;
            // lazyImage.srcset = lazyImage.dataset.srcset;
            lazyImage.classList.remove("lazy");

            lazyImages = lazyImages.filter(function(image) {
              return image !== lazyImage;
            });

            if (lazyImages.length === 0) {
              document.removeEventListener("scroll", lazyLoad);
              window.removeEventListener("resize", lazyLoad);
              window.removeEventListener("orientationchange", lazyLoad);
            }
          }
        });

        active = false;
      }, 200);
    }
  };

  document.addEventListener("scroll", lazyLoad);
  window.addEventListener("resize", lazyLoad);
  window.addEventListener("orientationchange", lazyLoad);
});

// document.addEventListener("DOMContentLoaded", function() {
//   var lazyBackgrounds = [].slice.call(document.querySelectorAll(".lazy-background"));

//   if ("IntersectionObserver" in window) {
//     let lazyBackgroundObserver = new IntersectionObserver(function(entries, observer) {
//       entries.forEach(function(entry) {
//         if (entry.isIntersecting) {
//           entry.target.classList.add("visible");
//           lazyBackgroundObserver.unobserve(entry.target);
//         }
//       });
//     });

//     lazyBackgrounds.forEach(function(lazyBackground) {
//       lazyBackgroundObserver.observe(lazyBackground);
//     });
//   }
// });

(function($) {
    $(window).on('load', function(){
      $('.preloader__wrapper').addClass('hide');
      /*Phone block*/
      $('.phone__btn').on('click', function(){
        let menuWrapper = $('.menu__wrapper'),
            phoneBlock  = $('.phone__popup');

        phoneBlock.addClass('show');
        menuWrapper.addClass('show');
      });

      /*Main menu*/
      $('.menu__btn').on('click', function(){
        let menuWrapper = $('.menu__wrapper'),
            menuBlock = $('.menu__block'),
            thisMenu = $(this);

        menuBlock.removeClass('show');
        if( thisMenu.hasClass('category__nav') ){
          menuWrapper.addClass('show');
          $('.category__menu').addClass('show');
        } else if(thisMenu.hasClass('main__nav')){
          menuWrapper.addClass('show');
          $('.main__menu').addClass('show');
        }
      });

      $('.menu__wrapper').on('click', function(){
        $('.menu__block').removeClass('show');
        $(this).removeClass('show');
      });

      $('.menu__block .close__menu').on('click', function(){
        $('.menu__block').removeClass('show');
        $('.menu__wrapper').removeClass('show');
        $('.phone__popup').removeClass('show');
      });

      /*Fullscrenn slider*/
      if($('.fullscreen__slider').length){
        $('.fullscreen__slider').slick({
            infinite:       true,
            autoplay:       true,
            autoplaySpeed:  8000,
            speed:          500,
            arrows:         false,
            dots:           true,
            fade:           true,
            cssEase:        'linear'
        });
      }

      /*Testimonials*/
      if($('.testimonial__slider').length){
        $('.testimonial__slider').slick({
            infinite:       true,
            autoplay:       true,
            autoplaySpeed:  8000,
            speed:          1500,
            arrows:         true,
            dots:           true,
            fade:           false
        });
      }

      /*Partners*/
      if($('.partners__slider').length){
        $('.partners__slider').slick({
            infinite:       true,
            autoplay:       true,
            autoplaySpeed:  8000,
            speed:          1500,
            arrows:         true,
            dots:           true,
            fade:           false,
            slidesToShow:   5,
            responsive: [
              {
                  breakpoint: 1367,
                  settings: {
                      slidesToShow: 4
                  }
              },
              {
                  breakpoint: 991,
                  settings: {
                      slidesToShow: 3
                  }
              },
              {
                  breakpoint: 767,
                  settings: {
                      slidesToShow: 2
                  }
              },
              {
                  breakpoint: 575,
                  settings: {
                      slidesToShow: 1
                  }
              }
          ]
        });
      }

      /*Product slider*/
      if($('.image__slider').length){
        let $imageSlider = {
              infinite:       false,
              autoplay:       false,
              speed:          1000,
              arrows:         true,
              dots:           false,
              fade:           false,
              slidesToShow:   1,
              asNavFor:       '.thumb__slider'
            },
            $thumbSlider = {
              infinite:       false,
              autoplay:       false,
              speed:          1000,
              arrows:         false,
              dots:           false,
              fade:           false,
              slidesToShow:   3,
              slidesToScroll: 1,
              centerMode:     false,
              focusOnSelect:  true,
              centerPadding:  0,
              asNavFor:       '.image__slider'
            };

        $('.image__slider').slick($imageSlider);
        $('.thumb__slider').slick($thumbSlider);
      }

      if($('.content__image').length){
        $('.content__image').slick({
            infinite:       true,
            autoplay:       false,
            speed:          1500,
            arrows:         false,
            dots:           true,
            fade:           false,
            slidesToShow:   1
        });
      }

      if($('.additional__slider').length){
        $('.additional__slider').slick({
            infinite:       true,
            autoplay:       false,
            speed:          1500,
            arrows:         false,
            dots:           true,
            fade:           true,
            slidesToShow:   1
        });
      }

      if($('.accessories__slider').length){
        $('.accessories__slider').slick({
            infinite:       true,
            autoplay:       false,
            speed:          1500,
            arrows:         true,
            dots:           false,
            fade:           false,
            slidesToShow:   3,
            slidesToScroll: 1,
            responsive: [
              {
                  breakpoint: 767,
                  settings: {
                      slidesToShow: 2
                  }
              },
              {
                  breakpoint: 575,
                  settings: {
                      slidesToShow: 1
                  }
              }
            ]
        });
      }

      /*Widget mobile*/
      $('.sidebar__widgets.mobile .widget h5').on('click', function(){
        var widget = $(this).closest('.widget').find('div');
        
        $(this).toggleClass('show');
        widget.slideToggle(300);
      });

      /*Tab*/
      $('.tab__wrapper li').on('click', function(){
        var tabIndex = $(this).index(),
            content  = $(this).closest('.tab__wrapper').find('.content .tab');

        $('.tab__wrapper li').removeClass('active');
        $(this).addClass('active');

        $('.content .tab').removeClass('active');
        content.eq(tabIndex).addClass('active');
      });

      /*Show more*/
      $('.show__details').on('click', function(e){
        e.preventDefault();
        $(this).addClass('hide');
        $(this).parent().find('.full_description').removeClass('hide');
      });

      $(function() {
          var marker = [], infowindow = [], map, image = $('.map__wrapper').attr('data-marker');

          function addMarker(location,name){
              marker[name] = new google.maps.Marker({
                  position: location,
                  map: map,
                  icon: image
              });
              marker[name].setMap(map);

              // infowindow[name] = new google.maps.InfoWindow({
              //     content:contentstr
              // });
              
              // google.maps.event.addListener(marker[name], 'click', function() {
              //     infowindow[name].open(map,marker[name]);
              // });
          }

          function initialize() {

              var lat = $('#map-canvas').attr("data-lat");
              var lng = $('#map-canvas').attr("data-lng");
              // var mapStyle = googleMapStyle;

              var myLatlng = new google.maps.LatLng(lat,lng);

              var setZoom = parseInt($('#map-canvas').attr("data-zoom"));
              
              // var styledMap = new google.maps.StyledMapType(mapStyle,{name: "Styled Map"});

              var mapOptions = {
                  zoom: setZoom,
                  disableDefaultUI: false,
                  scrollwheel: false,
                  zoomControl: true,
                  streetViewControl: true,
                  center: myLatlng
              };
              map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
              
              // map.mapTypes.set('map_style', styledMap);
              // map.setMapTypeId('map_style');
              

              // $('.location__list a').each(function(){
                  // var mark_lat = $(this).attr('data-lat');
                  // var mark_lng = $(this).attr('data-lng');
                  // var this_index = $('.location__list a').index(this);
                  // var mark_name = 'template_marker_'+this_index;
                  var mark_locat = new google.maps.LatLng(lat, lng);
                  // var mark_str = $(this).attr('data-string');;
                  addMarker(mark_locat,1);   
              // });   
          }

          
          if ($('.map__wrapper').length){    
              setTimeout(function(){
                  initialize();
              }, 500);
          }
      });

      /*Banner scrolling*/
      $('.fullscreen__banner .arrow, .products__banner .arrow').on('click', function(){
          let windowHeight = $(window).height();
          $('html, body').animate({ scrollTop: windowHeight }, 600);
      });

      /*Tabs*/
      $('.tabs__title .title').on('click', function(){
        var thisTab = $('.tabs__title .title').index(this);

        $(this).addClass('active');
        $('.tabs__title .title').not(this).removeClass('active');
        $('.tabs__content .tab').removeClass('active');
        $('.tabs__content .tab').eq(thisTab).addClass('active');
      });

      /*Products load*/
      var cat,
          maxPageCount,
          searchKey,
          page = 1;

      $('.load__products').on('click', function(){
        maxPageCount = parseInt($('.products__wrapper').data('max-page'));

        $(this).addClass('load');
        $('.products__wrapper').addClass('load');
        cat = $('.products__wrapper').data('cat');

        // console.log(cat, page);

        $.post( ajaxurl, {
            'action'        : 'oazys_load_products',
            'category_id'   : cat,
            'page'          : page
        })
        .done(function(response) {
            $('.products__wrapper .row').append(response);
            page++;
            if(maxPageCount <= page) {
                $('.load__products').addClass('disable');
            } else {
                $('.load__products').removeClass('disable');
            }
            $('.products__wrapper').removeClass('load');
            $('.load__products').removeClass('load');
        });
      });

      /*Post load*/
      $('.load__posts').on('click', function(){
        maxPageCount = parseInt($('.products__wrapper').data('max-page'));

        $(this).addClass('load');
        $('.products__wrapper').addClass('load');
        cat = $('.products__wrapper').data('cat');

        // console.log(cat, page);

        $.post( ajaxurl, {
            'action'        : 'oazys_load_posts',
            'page'          : page
        })
        .done(function(response) {
            $('.products__wrapper .row').append(response);
            page++;
            if(maxPageCount <= page) {
                $('.load__posts').addClass('disable');
            } else {
                $('.load__posts').removeClass('disable');
            }
            $('.products__wrapper').removeClass('load');
            $('.load__posts').removeClass('load');
        });
      });

    	AOS.init();
    });
})(jQuery);